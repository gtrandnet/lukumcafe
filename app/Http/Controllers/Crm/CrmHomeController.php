<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CrmHomeController extends Controller
{
    public function index()
    {
        return response()->json([
            'data' => 'Crm index'
        ], 200);
    }
}
