export const orders = ({ dispatch }, { context }) => {
    return axios.get('/api/orders').then((response) => {
        context.orders = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}